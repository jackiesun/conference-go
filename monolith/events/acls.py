import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}

    params = {"per_page": 1,
              "query": city + " " + state}

    url = "http://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)

    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}



def get_weather_data(city, state):

    # GETTING GEO DATA
    url = "http://api.openweathermap.org/geo/1.0/direct"
    code = "USA"
    # use params to get geo data:
    params = {
        "q": city + ", " + state + ", " + code,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    # # create a dictionary of data to use:
    try:

        lat = content[0]["lat"]
        lon = content[0]["lon"]

        # GETTING WEATHER DATA
        url = "https://api.openweathermap.org/data/2.5/weather"
        params2 = {
            "lat": lat,
            "lon": lon,
            "appid": OPEN_WEATHER_API_KEY,
            "units": "imperial",
        }
        response2 = requests.get(url, params=params2)

        content2 = json.loads(response2.content)

        weather = {
            "temp": int(content2["main"]["temp"]),
            "description": content2["weather"][0]["description"],
        }

        return weather
    except:
        return None

 # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
